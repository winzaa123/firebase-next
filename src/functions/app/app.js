import * as functions from "firebase-functions"
import next from "next"
const dev = process.env.NODE_ENV !== "production"
const app = next({ dev, conf: { distDir: "next" } })
const handle = app.getRequestHandler()
const express = require('express')
const cors = require("cors")
const appServer = express()

const routesAPI = require('../api')
appServer.use(cors({ origin: true }))

appServer.get("/xml", (req, res) => {
    res.set("Content-Type", "text/xml");
    res.send(
       `<?xml version="1.0" encoding="UTF-8"?>
  <breakfast_menu>
  
  <food>
  <name>Belgian Waffles</name>
  <price>$5.95</price>
  <description>Two of our famous Belgian Waffles with plenty of real maple syrup</description>
  <calories>650</calories>
  </food>
  
  <food>
  <name>Strawberry Belgian Waffles</name>
  <price>$7.95</price>
  <description>Light Belgian waffles covered with strawberries and whipped cream</description>
  <calories>900</calories>
  </food>
  
  <food>
  <name>Berry-Berry Belgian Waffles</name>
  <price>$8.95</price>
  <description>Light Belgian waffles covered with an assortment of fresh berries and whipped cream</description>
  <calories>900</calories>
  </food>
  
  <food>
  <name>French Toast</name>
  <price>$4.50</price>
  <description>Thick slices made from our homemade sourdough bread</description>
  <calories>600</calories>
  </food>
  
  <food>
  <name>Homestyle Breakfast</name>
  <price>$6.95</price>
  <description>Two eggs, bacon or sausage, toast, and our ever-popular hash browns</description>
  <calories>950</calories>
  </food>
  
  </breakfast_menu>`
    );
  });
  appServer.use('/api', routesAPI)
  appServer.get('*', (req, res) => {
    console.log('hello')
    return app.prepare().then(() => handle(req, res))
  })



const nextApp = functions.https.onRequest((request, response) => {
  console.log("File: " + request.originalUrl)
  console.log("dev:", dev)
  if (!request.path) {
    request.url = `/${request.url}`; // prepend '/' to keep query params if any
  }

  // log the page.js file or resource being requested

  return appServer(request, response)
})

export { nextApp }
