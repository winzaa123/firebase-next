import React from "react"
import App from "../components/App"

export default () => (
  <App>
    <p className="example">Index Page</p>
  </App>
)
